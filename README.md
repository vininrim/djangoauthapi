<h1>API Авторизации на Django</h1>

<h2>Endpoint'ы</h2>
<ul>
    <li><b>api/v1/auth/login/</b> - Принимает запросы в Json формате и возвращает token, если запрос удачный</li>
    <li><b>api/v1/auth/registration/</b> - Принимает запросы в Json формате и возвращает token, если запрос удачный</li>
    <li><b>api/v1/auth/logout</b> - Разлогинивает пользователя</li>
</ul>
<p>
Также имеются и другие Endpont'ы. Их вы можете посмотреть <b>api/v1/auth/</b>
</p>

<p>
<h2>Внимание!!!</h2>
При установки библиотек будут ошибки в модулях, их придется фиксить.
url менять на re_path и т.д. Их можно легко загуглить
</p>

<p>Копирвовать проект из ветки <a href="https://gitlab.com/vininrim/djangoauthapi/-/tree/master">master</a>
Файл зависимостей находится в <a href="https://gitlab.com/vininrim/djangoauthapi/-/tree/master/ApiAuth">ветка master -> ApiAuth</a>


После копирования DjangoAuthApi на локальный носитель в дирректории DjangoAuthApi(djangoauthapi) выполнить команду
`python -m venv venv` затем активировать venv `venv/scripts/activate` и установить зависимости
`pip install -r ApiAuth/requirements.txt`.
Теперь можно запускать проект `python ApiAuth/manage.py runserver`
</p>
